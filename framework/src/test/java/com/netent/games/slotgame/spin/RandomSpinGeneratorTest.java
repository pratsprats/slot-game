package com.netent.games.slotgame.spin;

import org.testng.annotations.Test;

import com.netent.games.slotgame.entity.SpinOutput;

import junit.framework.Assert;

public class RandomSpinGeneratorTest {

	@Test
	public void testSpinAndGenerateRandomResult() {
		SpinOutput spinResult = RandomSpinGenerator.spinAndGenerateRandomResult();
		Assert.assertNotNull(spinResult);
	}
}
