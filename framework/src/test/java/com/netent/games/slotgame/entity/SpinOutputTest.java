package com.netent.games.slotgame.entity;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SpinOutputTest {
	
	@Test
	public void getSpinOutput(){
		Assert.assertEquals(SpinOutput.getSpinResult(1), SpinOutput.LUCKY_ONE);
		Assert.assertEquals(SpinOutput.getSpinResult(2), SpinOutput.LUCKY_TWO);
		Assert.assertEquals(SpinOutput.getSpinResult(9), SpinOutput.OOPS);
	}
}
