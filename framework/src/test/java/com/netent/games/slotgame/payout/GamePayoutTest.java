package com.netent.games.slotgame.payout;

import java.math.BigDecimal;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.netent.games.slotgame.entity.SpinOutput;

public class GamePayoutTest {

	@Test
	public void testGamePayoutSingleSpinWinner() {
		GamePayout gamePayout = new GamePayout(SpinOutput.LUCKY_EIGHT);
		Assert.assertEquals(gamePayout.isWinner(), true);
		Assert.assertEquals(gamePayout.getPayoutAmount(), new BigDecimal(0));
		Assert.assertEquals(gamePayout.isFreeGame(), false);
	}

	@Test
	public void testGamePayoutSingleSpinLoser() {
		GamePayout gamePayout = new GamePayout(SpinOutput.LUCKY_ONE);
		Assert.assertEquals(gamePayout.isWinner(), false);
		Assert.assertEquals(gamePayout.getPayoutAmount(), new BigDecimal(0));
		Assert.assertEquals(gamePayout.isFreeGame(), false);
	}

	@Test
	public void testGamePayoutDoubleSpinWinner() {
		GamePayout gamePayout = new GamePayout(SpinOutput.LUCKY_ONE, SpinOutput.LUCKY_EIGHT);
		Assert.assertEquals(gamePayout.isWinner(), true);
		Assert.assertEquals(gamePayout.getPayoutAmount(), new BigDecimal(20));
		Assert.assertEquals(gamePayout.isFreeGame(), true);
	}

	@Test
	public void testGamePayoutDoubleSpinLoser() {
		GamePayout gamePayout = new GamePayout(SpinOutput.LUCKY_EIGHT, SpinOutput.LUCKY_ONE);
		Assert.assertEquals(gamePayout.isWinner(), false);
		Assert.assertEquals(gamePayout.getPayoutAmount(), new BigDecimal(0));
		Assert.assertEquals(gamePayout.isFreeGame(), false);
	}

}
