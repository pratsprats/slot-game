package com.netent.games.slotgame.constant;

import java.math.BigDecimal;

/* Common constants used by all games. */
public interface SlotGameConstants {
	public static final int DEFAULT_STARTING_BALANCE = 0;
	public static final int VIRTUAL_REEL_NUMBER = 20;
	public static final BigDecimal ZERO = new BigDecimal(0);
	public static final BigDecimal TWENTY_COINS_PAYOUT = new BigDecimal(20);
	public static final int MAX_RANDOM_NUMBER = 1000000;
	public static final int REEL_SIZE = 3;
	public static final BigDecimal DEFAULT_BET_AMOUNT = new BigDecimal(10);
	public static final int FIVE_COINS_PAYOUT = 5;

	public static final int NUM_OF_BOXES = 5;
	public static final int BOX_GAME_END = -1;

}