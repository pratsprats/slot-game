package com.netent.games.slotgame.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

/* Entity representing a player. */
public class Player {

	private String name;
	private BigDecimal accountBalance = new BigDecimal(0);
	private BigDecimal totalAmountUsed = new BigDecimal(0);
	private BigDecimal totalAmountWon = new BigDecimal(0);

	public Player(String name, BigDecimal accountBalance) {
		this.name = name;
		this.accountBalance = accountBalance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public void incrementAccountBalance(BigDecimal amount) {
		accountBalance = accountBalance.add(amount);
		totalAmountWon = totalAmountWon.add(amount);
	}

	public void decrementAccountBalance(BigDecimal amount) {
		accountBalance = accountBalance.subtract(amount);
		totalAmountUsed = totalAmountUsed.add(amount);
	}

	public BigDecimal getTotalAmountUsed() {
		return totalAmountUsed;
	}

	public void setTotalAmountUsed(BigDecimal totalAmountUsed) {
		this.totalAmountUsed = totalAmountUsed;
	}

	public BigDecimal getTotalAmountWon() {
		return totalAmountWon;
	}

	public void setTotalAmountWon(BigDecimal totalAmountWon) {
		this.totalAmountWon = totalAmountWon;
	}

	public String getRtp() {
		BigDecimal rtpValue = totalAmountWon.multiply(new BigDecimal(100)).divide(totalAmountUsed, 2,
				RoundingMode.HALF_EVEN);
		return rtpValue.toString();
	}

	@Override
	public String toString() {
		return "Player [" + (name != null ? "name=" + name + ", " : "") + "accountBalance=" + accountBalance
				+ ", totalAmountUsed=" + totalAmountUsed + ", totalAmountWon=" + totalAmountWon + ", rtp=" + getRtp()
				+ "]";
	}

}
