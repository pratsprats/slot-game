package com.netent.games.slotgame;

/* Interface defining box picking game. */

public interface IBoxPickingGame {
	public int openBoxesAndCalculateTotalCoins();
}
