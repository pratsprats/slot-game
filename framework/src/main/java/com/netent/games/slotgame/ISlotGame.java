package com.netent.games.slotgame;

import com.netent.games.slotgame.entity.GameType;

/* Interface defining any slot game. */
public interface ISlotGame {

	public void spin(GameType gameType);

}
