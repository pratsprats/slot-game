package com.netent.games.slotgame.spin;

import java.util.Random;

import com.netent.games.slotgame.constant.SlotGameConstants;
import com.netent.games.slotgame.entity.SpinOutput;

/* This class is responsible for generating a random number and map it 
 * to one of the spin results based on defined probability. */
public class RandomSpinGenerator {

	public static SpinOutput spinAndGenerateRandomResult() {
		Random r = new Random();
		int num = r.nextInt(SlotGameConstants.MAX_RANDOM_NUMBER);
		num = num % SlotGameConstants.VIRTUAL_REEL_NUMBER;
		SpinOutput spinResult = SpinOutput.getSpinResult(num);
		return spinResult;
	}
}
