package com.netent.games.slotgame.payout;

import java.math.BigDecimal;

import com.netent.games.slotgame.constant.SlotGameConstants;
import com.netent.games.slotgame.entity.SpinOutput;

/*
* This class is responsible of deciding payout to the player based on spin result. 
* Game Payout could be in form of freegame, bonusgame or amount. 
 */

public class GamePayout {
	private BigDecimal payoutAmount;
	private boolean isWinner;
	private boolean isFreeGame;

	public GamePayout(SpinOutput sr1, SpinOutput sr2) {
		payoutAmount = SlotGameConstants.ZERO;
		calcualtePayout(sr1, sr2);
	}

	public GamePayout(SpinOutput sr1) {
		payoutAmount = SlotGameConstants.ZERO;
		calcualtePayout(sr1);
	}

	/*
	 * Probability of getting any result out of LUCKY_ONE, LUCKY_TWO,
	 * LUCKY_THREE, LUCKY_FOUR, LUCKY_FIVE and LUCKY_SIX will be 6/20 i.e 30%.
	 * And probability of getting any result out of LUCKY_SEVEN, LUCKY_EIGHT
	 * will be 2/20 i.e 10%.
	 */
	public void calcualtePayout(SpinOutput spinOutput1, SpinOutput spinOutput2) {
		switch (spinOutput1) {
		case LUCKY_ONE:
		case LUCKY_TWO:
		case LUCKY_THREE:
		case LUCKY_FOUR:
		case LUCKY_FIVE:
		case LUCKY_SIX:
			isWinner = true;
			payoutAmount = SlotGameConstants.TWENTY_COINS_PAYOUT;
			break;
		default:
			break;
		}

		switch (spinOutput2) {
		case LUCKY_SEVEN:
		case LUCKY_EIGHT:
			isFreeGame = true;
			break;
		default:
			break;
		}

	}

	/*
	 * Probability of getting any result out of LUCKY_SEVEN, LUCKY_EIGHT will be
	 * 2/20 i.e 10%.
	 */
	public void calcualtePayout(SpinOutput spinOutput) {
		switch (spinOutput) {
		case LUCKY_SEVEN:
		case LUCKY_EIGHT:
			isWinner = true;
			break;
		default:
			break;
		}
	}

	public boolean isWinner() {
		return isWinner;
	}

	public BigDecimal getPayoutAmount() {
		return payoutAmount;
	}

	public boolean isFreeGame() {
		return isFreeGame;
	}

	@Override
	public String toString() {
		return "Payout [payoutAmount=" + payoutAmount + ", winner=" + isWinner + ", isFreeGame=" + isFreeGame + "]";
	}

}