package com.netent.games.slotgame.entity;

/* Enum representing various reel results. */
/*  Use of virtual reel will accurately control the probability of getting any given result.
 * Probability of getting any result out of LUCKY_ONE, LUCKY_TWO, LUCKY_THREE, LUCKY_FOUR, LUCKY_FIVE
 * and LUCKY_SIX will be 6/20 i.e 30%. And probability of getting any result out of LUCKY_SEVEN, LUCKY_EIGHT
 * will be 2/20 i.e 10%.
*/
public enum SpinOutput {

	LUCKY_ONE("LUCKY1", 1), LUCKY_TWO("LUCKY2", 2), LUCKY_THREE("LUCKY3", 3), LUCKY_FOUR("LUCKY4", 4), LUCKY_FIVE(
			"LUCKY5", 5), LUCKY_SIX("LUCKY6", 6), LUCKY_SEVEN("LUCKY7", 7), LUCKY_EIGHT("LUCKY8", 8), OOPS("OOPS");

	private int spinResult;
	private String name;
	private static final int IMPOSSIBLE_MATCH = 9999;

	// constructors
	private SpinOutput(String name) {
		setSpin(name, IMPOSSIBLE_MATCH);
	}

	private SpinOutput(String name, int spinResult) {
		setSpin(name, spinResult);
	}

	private void setSpin(String name, int spinResult) {
		this.name = name;
		this.spinResult = spinResult;
	}

	public String getSpinName() {
		return this.name;
	}

	public static SpinOutput getSpinResult(int spin) {
		SpinOutput result = OOPS;
		for (SpinOutput type : values()) {
			if (spin == type.spinResult) {
				result = type;
				break;
			}
		}
		return result;
	}
}
