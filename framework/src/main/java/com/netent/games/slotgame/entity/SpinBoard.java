package com.netent.games.slotgame.entity;

import java.util.Arrays;

import com.netent.games.slotgame.constant.SlotGameConstants;

/* Entity representing a spin board with 3 reel. */
public class SpinBoard {
	private SpinOutput board[];

	public SpinBoard() {
		board = new SpinOutput[SlotGameConstants.REEL_SIZE];
	}

	/* Fill all the reels of given board with same spin output */
	public void fillBoard(SpinOutput spinOutput) {
		Arrays.fill(board, spinOutput);
	}

	/*
	 * Fill first two reels of given board with spinoutput1 and last reel with
	 * spinOutput2.
	 */
	public void fillBoard(SpinOutput spinOutput1, SpinOutput spinOutput2) {
		Arrays.fill(board, 0, 2, spinOutput1);
		Arrays.fill(board, 2, 3, spinOutput2);
	}

	/* Print spin board on console for debugging */
	public void printBoard() {
		System.out.printf("%s", "Slot Board:\n");
		System.out.println();

		System.out.printf("%10s | %10s | %10s\n", this.board[0].getSpinName(), this.board[1].getSpinName(),
				this.board[2].getSpinName());
	}
}
