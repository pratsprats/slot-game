package com.netent.games.slotgame.entity;

/* Enum defining types of available games. */
public enum GameType {
	NORMAL, FREE, BONUS;
}
