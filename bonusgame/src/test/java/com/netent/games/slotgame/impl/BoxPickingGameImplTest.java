package com.netent.games.slotgame.impl;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;

public class BoxPickingGameImplTest {
	@Mocked(stubOutClassInitialization = true)
	private Logger logger;

	@Test(priority = 1)
	public void testOpenBoxesAndCalculateTotalCoinsWithoutShuffle() {
		BoxPickingGameImpl boxPickingGameImpl = new BoxPickingGameImpl();

		new MockUp<Collections>() {
			@Mock
			public void shuffle(List<?> list) {
			}
		};

		int coins = boxPickingGameImpl.openBoxesAndCalculateTotalCoins();
		Assert.assertEquals(coins, 0);
	}

	@Test(priority = 2)
	public void testOpenBoxesAndCalculateTotalCoinsWithShuffle() {
		BoxPickingGameImpl boxPickingGameImpl = new BoxPickingGameImpl();

		int coins = boxPickingGameImpl.openBoxesAndCalculateTotalCoins();
		Assert.assertTrue(coins >= 0);
	}

}
