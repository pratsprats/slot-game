package com.netent.games.slotgame.impl;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.testng.annotations.Test;

import com.netent.games.slotgame.IBoxPickingGame;
import com.netent.games.slotgame.entity.GameType;
import com.netent.games.slotgame.entity.Player;
import com.netent.games.slotgame.entity.SpinBoard;
import com.netent.games.slotgame.payout.GamePayout;

import junit.framework.Assert;
import mockit.Injectable;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;

public class BonusGameImplTest {

	@Mocked(stubOutClassInitialization = true)
	private Logger logger;

	@Injectable
	private SpinBoard spinBoard;

	@Mocked
	private GamePayout gamePayout;

	@Injectable
	private BigDecimal betAmount;

	@Mocked
	private IBoxPickingGame boxPickingGame;

	@Test
	public void testSpinWinner() {

		final Player p = new Player("abc", new BigDecimal(100));
		BonusGameImpl b = new BonusGameImpl(p, new BigDecimal(10));

		new MockUp<BonusGameImpl>() {
			@Mock
			public boolean isWinner() {
				return true;
			}

			@Mock
			public void startBoxPickingGame() {
				int coinsWon = 5;
				p.incrementAccountBalance(new BigDecimal(coinsWon));
			}
		};

		b.spin(GameType.NORMAL);
		Assert.assertEquals(p.getAccountBalance(), new BigDecimal(95));
	}

	@Test
	public void testSpinLoser() {
		final Player p = new Player("abc", new BigDecimal(100));
		BonusGameImpl b = new BonusGameImpl(p, new BigDecimal(10));

		new MockUp<BonusGameImpl>() {
			@Mock
			public boolean isWinner() {
				return false;
			}

			@Mock
			public void startBoxPickingGame() {
				int coinsWon = 5;
				p.incrementAccountBalance(new BigDecimal(coinsWon));
			}
		};

		b.spin(GameType.NORMAL);
		Assert.assertEquals(p.getAccountBalance(), new BigDecimal(90));
	}

}
