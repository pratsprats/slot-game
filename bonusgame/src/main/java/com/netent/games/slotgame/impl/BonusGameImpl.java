package com.netent.games.slotgame.impl;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netent.games.slotgame.IBoxPickingGame;
import com.netent.games.slotgame.ISlotGame;
import com.netent.games.slotgame.entity.GameType;
import com.netent.games.slotgame.entity.Player;
import com.netent.games.slotgame.entity.SpinBoard;
import com.netent.games.slotgame.entity.SpinOutput;
import com.netent.games.slotgame.payout.GamePayout;
import com.netent.games.slotgame.spin.RandomSpinGenerator;

/* 
 * Implementation of bonus slot game. In a normal game round, the player has a 10% chance of triggering a bonus round. 
 * If the player wins a bonus round, they will play a separate “box picking game”. 
 * In this game the player is given 5 boxes. One of the boxes will end the box picking
 * game, while the rest contain 5 coins each. The player is allowed to open boxes 
 * until the game ends. The values should be assigned to the boxes randomly each 
 * time the player wins the bonus round. The player should not be able to win coins by any other means.
 */
public class BonusGameImpl implements ISlotGame {
	private static Logger logger = LoggerFactory.getLogger(BonusGameImpl.class);

	private SpinBoard spinBoard;
	private Player player;
	private BigDecimal betAmount;
	private boolean isWinner;
	private IBoxPickingGame boxPickingGame;

	public BonusGameImpl(Player player, BigDecimal betAmount) {
		this.spinBoard = new SpinBoard();
		this.player = player;
		this.betAmount = betAmount;
	}

	/* Implementation of spin method. */
	public void spin(GameType gameType) {
		logger.debug("Starting Bonus Slot Game of type : " + gameType);

		/* Generate Spin and Spin Result */
		SpinOutput spinResult = generateSpin();

		/* Calculate payout based on spin result */
		GamePayout payout = calculatePayout(spinResult);

		/* Fill Spin board with appropriate result */
		fillBoard(payout, spinResult);

		/* Decrement bet amount from user's account */
		player.decrementAccountBalance(betAmount);

		/* Set winner */
		setWinner(payout.isWinner());

		/* In case player won the game, start box picking game. */
		if (isWinner()) {
			// spinBoard.printBoard();
			logger.debug("Player won the game : " + player + ". Box Picking Game will start now.");
			startBoxPickingGame();
		}
	}

	/* Box picking game will be triggered using this method */
	private void startBoxPickingGame() {
		boxPickingGame = new BoxPickingGameImpl();
		int coinsWon = boxPickingGame.openBoxesAndCalculateTotalCoins();
		player.incrementAccountBalance(new BigDecimal(coinsWon));
	}

	/* Calculate payout based on spin result */
	private GamePayout calculatePayout(SpinOutput spinResult) {
		return new GamePayout(spinResult);
	}

	/* Fill spin board with appropriate result */
	private void fillBoard(GamePayout payout, SpinOutput spinResult) {
		if (payout.isWinner()) {
			spinBoard.fillBoard(spinResult);
		} else {
			spinBoard.fillBoard(SpinOutput.OOPS);
		}
	}

	/*
	 * Generate Random number and spin result for triggering box picking game
	 */
	private SpinOutput generateSpin() {
		return RandomSpinGenerator.spinAndGenerateRandomResult();
	}

	public boolean isWinner() {
		return isWinner;
	}

	private void setWinner(boolean isWinner) {
		this.isWinner = isWinner;
	}

}
