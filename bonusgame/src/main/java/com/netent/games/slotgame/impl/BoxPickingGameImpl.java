package com.netent.games.slotgame.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netent.games.slotgame.IBoxPickingGame;
import com.netent.games.slotgame.constant.SlotGameConstants;

/* Implementation of Box Picking game. In this game the player is given 5 boxes. 
 * One of the boxes will end the box picking game, while the rest contain 5 coins each.
 * The player is allowed to open boxes until the game ends. The values should be assigned
 * to the boxes randomly each time the player wins the bonus round. */
public class BoxPickingGameImpl implements IBoxPickingGame {
	private static Logger logger = LoggerFactory.getLogger(BoxPickingGameImpl.class);

	private static List<Integer> boxes = new ArrayList<Integer>();

	static {
		initBoxes();
	}

	/* Initialize boxes */
	private static void initBoxes() {
		boxes.add(SlotGameConstants.BOX_GAME_END);
		for (int index = 1; index < SlotGameConstants.NUM_OF_BOXES; index++) {
			boxes.add(SlotGameConstants.FIVE_COINS_PAYOUT);
		}
	}

	/* Shuffle, open boxes sequentially and calculate total coins earned. */
	public int openBoxesAndCalculateTotalCoins() {
		Collections.shuffle(boxes);
		int totalCoinsEarned = 0;
		for (int index = 0; index < SlotGameConstants.NUM_OF_BOXES; index++) {
			Integer boxValue = boxes.get(index);

			if (boxValue.equals(SlotGameConstants.BOX_GAME_END)) {
				logger.debug("Box Picking Game ends. Bye bye.");
				break;
			} else {
				logger.debug("Incrementing coins from the box picked.");
				totalCoinsEarned = totalCoinsEarned + boxValue;
			}
		}
		return totalCoinsEarned;
	}
}
