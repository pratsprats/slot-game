# README #

### What is this repository for? ###

* Quick summary - Backend server for online slot game.
* Version - v1
* Code coverage report - Run "mvn clean cobertura:cobertura" and check report at location - target/site/cobertura/index.html. 
* Current code coverage - slotgame:88%, bonusgame:88%, framework:57%
* Steps for code checkout, build and mock game execution - 
	1. git clone https://pratsprats@bitbucket.org/pratsprats/slot-game.git
	2. cd slot-game
	3. mvn clean install
	4. cd slotgame
	5. mvn exec:java -Dexec.mainClass="com.netent.games.slotgame.runner.MockSlotGame"
	6. cd ../bonusgame
	7. mvn exec:java -Dexec.mainClass="com.netent.games.slotgame.runner.MockBonusGame"


* Sample Output from Mock Slot game - 
	1. Player 1 stats : Player [name=Prats, accountBalance=-3336080, totalAmountUsed=10000000, totalAmountWon=6663920, rtp=66.64]
	2. Player 2 stats : Player [name=John, accountBalance=-3342720, totalAmountUsed=10000000, totalAmountWon=6657280, rtp=66.57]
	3. Player 3 stats : Player [name=Mark, accountBalance=-3326120, totalAmountUsed=10000000, totalAmountWon=6673880, rtp=66.74]
	
* Sample output from Mock bonus game - 
	1. Player 1 stats : Player [name=Prats, accountBalance=-8998340, totalAmountUsed=10000000, totalAmountWon=1001660, rtp=10.02]
	2. Player 2 stats : Player [name=John, accountBalance=-9001785, totalAmountUsed=10000000, totalAmountWon=998215, rtp=9.98]
	3. Player 3 stats : Player [name=Mark, accountBalance=-8996995, totalAmountUsed=10000000, totalAmountWon=1003005, rtp=10.03]

* Theoretical RTP for Slot Game - 
	1. Assuming 100 games, Total coins for betting - 100 games * 10 coins = 1000 coins
	2. 30% chance of winning 20 coins back - ((30*100)/100) games * 20 coins = 600 coins
	3. 10% chance of triggering free round, which will again trigger Point 2 - (((10*100)/100) * 30)/100 = 3 games * 20 coins = 60 coins
	4. So, RTP = (Total coins won/Total coins used for betting) = (600+60)/1000 = 0.66 = 66%
	
* Theoretical RTP for Bonus Game - 
	1. Assuming 100 games, Total coins for betting - 100 games * 10 coins = 1000 coins
	2. 10% chance of triggering box picking round (10*100)/100 = 10 games
	3. Probability of winning 0 coins = 1/5, probability of winning 5 coins = 4/5, Probability of winning 10 coins = 3/5, Probability of winning 15 coins = 2/5, Probability of winning 20 coins = 1/5
	4. But winning 20 will mean he has won 5, 10, 15 also and so on.
	5. So Probability of winning 5 coins = 1/5, Probability of winning 10 coins = 1/5, Probability of winning 15 coins = 1/5, Probability of winning 20 coins = 1/5
	6. (1/5)*5 + (1/5)*10 + (1/5)*15 + (1/5)*20 =1+2+3+4=10 
	7. Total coins won = 10 games * 10 coins = 100 coins
	8. So, RTP = (Total coins won/Total coins used for betting) = 100/1000 = 0.1 = 10%
	
	
