package com.netent.games.slotgame.impl;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.testng.annotations.Test;

import com.netent.games.slotgame.entity.GameType;
import com.netent.games.slotgame.entity.Player;
import com.netent.games.slotgame.entity.SpinBoard;
import com.netent.games.slotgame.payout.GamePayout;

import junit.framework.Assert;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;

public class SlotGameImplTest {

	@Mocked(stubOutClassInitialization = true)
	private Logger logger;

	@Injectable
	private SpinBoard spinBoard;

	@Mocked
	private GamePayout gamePayout;

	@Injectable
	private BigDecimal betAmount;

	@Test
	public void testSpinWinner() {

		final Player p = new Player("abc", new BigDecimal(100));
		SlotGameImpl b = new SlotGameImpl(p, new BigDecimal(10));

		new MockUp<SlotGameImpl>() {
			@Mock
			public boolean isWinner() {
				return true;
			}

			@Mock
			public boolean isFreeGame() {
				return false;
			}
		};

		new Expectations() {
			{
				gamePayout.getPayoutAmount();
				result = new BigDecimal(50);
			}
		};

		b.spin(GameType.NORMAL);
		Assert.assertEquals(p.getAccountBalance(), new BigDecimal(140));
	}

	@Test
	public void testSpinLoser() {

		final Player p = new Player("abc", new BigDecimal(100));
		SlotGameImpl b = new SlotGameImpl(p, new BigDecimal(10));

		new MockUp<SlotGameImpl>() {
			@Mock
			public boolean isWinner() {
				return false;
			}

			@Mock
			public boolean isFreeGame() {
				return false;
			}
		};

		new Expectations() {
			{
				gamePayout.getPayoutAmount();
				result = new BigDecimal(50);
				times = 0;
			}
		};

		b.spin(GameType.FREE);
		Assert.assertEquals(p.getAccountBalance(), new BigDecimal(100));
	}
}
