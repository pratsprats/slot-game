package com.netent.games.slotgame.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netent.games.slotgame.ISlotGame;
import com.netent.games.slotgame.entity.GameType;
import com.netent.games.slotgame.entity.Player;
import com.netent.games.slotgame.entity.SpinBoard;
import com.netent.games.slotgame.entity.SpinOutput;
import com.netent.games.slotgame.payout.GamePayout;
import com.netent.games.slotgame.spin.RandomSpinGenerator;

/* Implementation of slot game. In any round (free or normal), the player has a 30 % chance of winning back 20
coins. In any round (free or normal), the player also has a 10% chance of triggering a
free round where the player does not have to pay for bet. The free round works in
the same way as a normal round except it costs 0 coins. The free round should
follow immediately after the normal round. The player can both win coins and free round at the same time
*/
public class SlotGameImpl implements ISlotGame {
	private static Logger logger = LoggerFactory.getLogger(SlotGameImpl.class);

	private SpinBoard spinBoard;
	private Player player;
	private BigDecimal betAmount;
	private boolean isWinner;
	private boolean isFreeGame;

	public SlotGameImpl(Player player, BigDecimal betAmount) {
		this.spinBoard = new SpinBoard();
		this.player = player;
		this.betAmount = betAmount;
	}

	/* Implementation of spin method. */
	public void spin(GameType gameType) {
		logger.debug("Starting Slot Game of type : " + gameType);

		/* Generate Spin and Spin Result */
		List<SpinOutput> spinOutput = generateSpin();

		/* Calculate payout based on spin result */
		GamePayout payout = calculatePayout(spinOutput);

		/* Fill Spin board with appropriate result */
		fillBoard(payout, spinOutput);

		/* Decrement bet amount from user's account if game type is not Free */
		if (!gameType.equals(GameType.FREE)) {
			player.decrementAccountBalance(betAmount);
		}

		/* Set winner and free game */
		setWinner(payout.isWinner());
		setFreeGame(payout.isFreeGame());

		/* Increment payout amount to user's account if player won the game */
		if (isWinner()) {
			// spinBoard.printBoard();
			logger.debug("Player won the game : " + player);
			player.incrementAccountBalance(payout.getPayoutAmount());
		}

		/*
		 * If player won Free round as well, start the same game with Free type
		 */
		if (isFreeGame()) {
			// spinBoard.printBoard();
			logger.debug("Player won free round. Spining for FREE game now");
			spin(GameType.FREE);
		}

	}

	/* Calculate payout based on spin results */
	private GamePayout calculatePayout(List<SpinOutput> spinResults) {
		return calculatePayout(spinResults.get(0), spinResults.get(1));
	}

	/* Calculate payout based on spin results */
	private GamePayout calculatePayout(SpinOutput result1, SpinOutput result2) {
		return new GamePayout(result1, result2);
	}

	/* Fill spin board with appropriate result */
	private void fillBoard(GamePayout payout, List<SpinOutput> spinResults) {
		fillBoard(payout, spinResults.get(0), spinResults.get(1));
	}

	/* Fill spin board with appropriate result */
	private void fillBoard(GamePayout payout, SpinOutput result1, SpinOutput result2) {
		if (payout.isWinner()) {
			spinBoard.fillBoard(result1);
		} else if (payout.isFreeGame()) {
			spinBoard.fillBoard(result2, result1);
		} else {
			spinBoard.fillBoard(SpinOutput.OOPS);
		}
	}

	/*
	 * Generate Random number and spin result twice. First one for winning coins
	 * and other one for triggering free round.
	 */
	private List<SpinOutput> generateSpin() {
		List<SpinOutput> spinResults = new ArrayList<SpinOutput>();
		for (int i = 0; i < 2; i++) {
			SpinOutput spinResult = RandomSpinGenerator.spinAndGenerateRandomResult();
			spinResults.add(spinResult);
		}
		return spinResults;
	}

	public boolean isWinner() {
		return isWinner;
	}

	private void setWinner(boolean isWinner) {
		this.isWinner = isWinner;
	}

	public boolean isFreeGame() {
		return isFreeGame;
	}

	private void setFreeGame(boolean isFreeGame) {
		this.isFreeGame = isFreeGame;
	}
}