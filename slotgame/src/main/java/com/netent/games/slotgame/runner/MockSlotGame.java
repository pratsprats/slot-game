package com.netent.games.slotgame.runner;

import java.math.BigDecimal;

import com.netent.games.slotgame.ISlotGame;
import com.netent.games.slotgame.constant.SlotGameConstants;
import com.netent.games.slotgame.entity.GameType;
import com.netent.games.slotgame.entity.Player;
import com.netent.games.slotgame.impl.SlotGameImpl;

/* Main class to run slot game for 3 players and print RTP. Number of games - 1000000*/
public class MockSlotGame {

	public static void main(String[] args) {
		Player player = createPlayer1();

		ISlotGame game = new SlotGameImpl(player, SlotGameConstants.DEFAULT_BET_AMOUNT);
		for (int i = 0; i < 1000000; i++) {
			game.spin(GameType.NORMAL);
		}
		System.out.println("Player 1 stats : " + player);

		player = createPlayer2();

		game = new SlotGameImpl(player, SlotGameConstants.DEFAULT_BET_AMOUNT);
		for (int i = 0; i < 1000000; i++) {
			game.spin(GameType.NORMAL);
		}
		System.out.println("Player 2 stats : " + player);

		player = createPlayer3();

		game = new SlotGameImpl(player, SlotGameConstants.DEFAULT_BET_AMOUNT);
		for (int i = 0; i < 1000000; i++) {
			game.spin(GameType.NORMAL);
		}
		System.out.println("Player 3 stats : " + player);
	}

	private static Player createPlayer1() {
		return new Player("Prats", new BigDecimal(0));
	}

	private static Player createPlayer2() {
		return new Player("John", new BigDecimal(0));
	}

	private static Player createPlayer3() {
		return new Player("Mark", new BigDecimal(0));
	}

}
